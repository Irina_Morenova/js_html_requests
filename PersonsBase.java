package com.artezio.demo.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PersonsBase{
    List<Person> people;

    public PersonsBase(){
        people = new ArrayList<>();

        for (int i = 0; i < 50; i++){
            Person person = new Person();
            person.setAge((int) (Math.random() * 50));
            people.add(person);
        }
        people.sort(Comparator.comparing(person -> person.getPets().get(0).ordinal()));
    }

    public List<String> getSample(String gender, String pet){
        List<String> p = new ArrayList<>();
        people.forEach(person -> {
            if ((gender.equals("") || person.getGender().equals(gender)) & (pet.equals("") || person.getPets().contains(Pet.valueOf(pet)))){
                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String json = "";
                try{
                    json = ow.writeValueAsString(person);
                } catch (JsonProcessingException e){
                    e.printStackTrace();
                }
                p.add(json);
            }
        });
        return p;
    }

}
