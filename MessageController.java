package com.artezio.demo.controller;

import com.artezio.demo.dao.Message;
import com.artezio.demo.dao.Person;
import com.artezio.demo.dao.PersonsBase;
import com.artezio.demo.repository.MessageCrudRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("message")
public class MessageController {

    @Autowired
    MessageCrudRepo repository;
    PersonsBase personsBase;

    public MessageController(){
        personsBase = new PersonsBase();
    }

    @GetMapping
    public List<Message> list() {
        return repository.findTop100ByOrderByIdDesc();
    }

    @GetMapping("person")
    public List<String> allList() {
        return personsBase.getSample("","");
    }

    @GetMapping("person/filter")
    public List<String> getSample(@RequestParam String gender, String pet) {
        return personsBase.getSample(gender, pet);
    }


    @GetMapping("{id}")
    public Message getOne(@PathVariable String id) {
        return repository.findById(Long.valueOf(id)).get();
    }

    @GetMapping("search_")
    public String getAuthor(@RequestBody String word) {
        List<String> authors = new ArrayList<>();
        List<Message> messages = repository.findAll();
        messages.forEach(m -> {
            if (m.getText().contains(word)) {
                if (m.getUsername().length() == 0) {
                    authors.add("Имя не указано");
                } else {
                    authors.add(m.getUsername());
                }
            }
        });
        if (authors.size() == 0) {
            return "Совпадений не найдено";
        } else {
            return "Совпадаения найдены у: " + authors.toString();
        }
    }

    @PostMapping
    public List<Message> add(@RequestBody Map<String, String> model, HttpServletRequest request) {
        String text = model.get("text");
        String username = model.get("username");
        if (text != null && !text.isEmpty()) {
            repository.save(new Message(text, username));
        }
        return repository.findTop100ByOrderByIdDesc();
    }

    @PostMapping("search")
    public String autors(@RequestBody String word){
        return getAuthor(word);
    }

    @PutMapping("{id}")
    public Message update(@PathVariable String id, @RequestBody Map<String, String> message) {
        Message messageFromDb = repository.findById(Long.valueOf(id)).get();
        String text = message.get("text");
        String username = message.get("username");
        if (text != null && !text.isEmpty()) {
            messageFromDb.setText(text);
        }

        if (username != null && !username.isEmpty()) {
            messageFromDb.setUsername(username);
        }
        messageFromDb.setId(Long.valueOf(id));
        repository.save(messageFromDb);
        return messageFromDb;
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        Message messageFromDb = repository.findById(Long.valueOf(id)).get();
        repository.delete(messageFromDb);
    }


}
