package com.artezio.demo.dao;

import java.util.*;

public class Person{
    private String age;
    private String gender;
    private List<Pet> pets;
    private Map<String, String> parameter;

    public Person(){
        pets = new ArrayList<>();
        parameter = new HashMap<>();
        parameter.put("height", String.format("%.2f", Math.random() * 2));
        parameter.put("weight", String.format("%.2f", 3 + Math.random() * 197));
        parameter.put("amount", String.format("%.2f", 20 + Math.random() * 160));

    }

    public void setAge(int age){
        this.age = String.valueOf(age);
        gender = age % 2 == 0 ? "Female" : "Male";
        if (age % 5 == 0) addPet(Pet.Alligator);
        if (age % 2 == 0) addPet(Pet.Cat);
        if (age % 3 == 0) addPet(Pet.Dog);
    }

    public void addPet(Pet pet){
        pets.add(pet);
        pets.sort(Comparator.comparing(Pet::ordinal));
    }

    public String getGender(){
        return gender;
    }

    public List<Pet> getPets(){
        if (pets.size()==0){
            List<Pet>ret = new ArrayList<>();
            ret.add(Pet.noPet);
            return ret;
        }
        return pets;
    }

    public String getAge(){
        return age;
    }

    public Map<String, String> getParameter(){
        return parameter;
    }

}
