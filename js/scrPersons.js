function GetAll() {
    var request = new XMLHttpRequest();
    request.open('GET', 'message/person', true);
    request.send();
    request.onload = function(){
        CreateTable(JSON.parse(request.responseText));
    }
}

function GetSample() {
    var selectGender = document.getElementById('selectGender').value;
    var selectPet = document.getElementById('selectPet').value;
    var request = new XMLHttpRequest();
    request.open('GET', 'message/person/filter?gender=' + selectGender + '&pet=' + selectPet, true);
    request.send();
    request.onload = function(){
        CreateTable(JSON.parse(request.responseText));
    }
}

function Sort(){
        var selectGender = document.getElementById('selectGender').value;
        var selectPet = document.getElementById('selectPet').value;
        var pattern = document.getElementById('pattern').value;
        var body = '{"pattern":"'+ pattern +'"}';
                   	console.log(body);
        var request = new XMLHttpRequest();
        request.open('POST', 'message/person/sort', true);
        request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        request.send(body);
        request.onload = function(){
        CreateTable(JSON.parse(request.responseText));
        }
}

function CreateTable(list) {
    Clear();
    var div = document.getElementById('tablePersons');

    if (list.length == 0) {
        div.innerHTML = "Совпадений не найдено";
        return;
    }
    div.innerHTML = '<table id ="persons" border="1px"><tr><th>Пол</th><th>Возраст</th><th>Питомец</th><th>Параметры</th></tr></table>';
    table = document.getElementById('persons');
    for (var i = 0; i < list.length; i++) {
        var line = document.createElement('tr');
        var node1 = document.createElement('td');
        var node2 = document.createElement('td');
        var node3 = document.createElement('td');
        var node4 = document.createElement('td');
        line.appendChild(node1);
        line.appendChild(node2);
        line.appendChild(node3);
        line.appendChild(node4);
        table.appendChild(line);

        var p = JSON.parse(list[i]);
        node1.innerHTML = p.gender;
        node2.innerHTML = p.age;
        node3.innerHTML = p.pets;
        node4.innerHTML = 'Рост - ' + p.parameter.height + 'м, Вес - ' + p.parameter.weight + 'кг, Объем талии - ' + p.parameter.amount;

    }
}

function Clear(){
    var table = document.getElementById('persons');
    if (table != null) {
        table.remove();
    }
}