function auto_add(){
//Почему поступают на сервер в случайном порядке?
    for (i = 0; i < 5; i++) {
    	var body = '{"username": "User' + i +'", "text": "my message for this chat"}';
    	var request = new XMLHttpRequest();
    	request.open("POST", 'message', true);
    	request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    	request.onreadystatechange = function(){};
    	request.send(body);
    }
}

auto_add();

var username = document.getElementsByClassName('username')[0];
var text = document.getElementsByClassName('text')[0];
var messagesArea = document.getElementById('messagesArea');
var field = document.createElement('div');
messagesArea.appendChild(field);
field.innerHTML = "...";

function GET_with_parameters(){
	var search_text = document.getElementById('search_text').value;
	var request = new XMLHttpRequest();
	request.open('GET', 'message/search?text=' + search_text, true);
	request.send();
	request.onload = function(){
        field.innerHTML = request.responseText;
    }

}
function GET_with_xml(){
	var search_text = document.getElementById('search_text').value;
	var request = new XMLHttpRequest();
	request.open("GET", "message/search/"+search_text, true);
	request.send();
    request.onload = function(){
        field.innerHTML = request.responseText;
    }
}

function POST_with_parameters(){
		var parameters = 'username=' + username.value +',text=' + text.value +'';
    	var request = new XMLHttpRequest();
    	request.open("POST", 'message', true);
    	request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    	request.onreadystatechange = function(){};
    	request.send(body);
}

function POST_with_body(){
	var body = '{"username": "' + username.value +
	'", "text": "' + text.value +'"}';
	var request = new XMLHttpRequest();
	request.open("POST", 'message', true);
	request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
	request.onreadystatechange = function(){};
	request.send(body);

}

function PUT_clear(){
    var id = document.getElementById('id').value;
    if (id != ""){
        var body = '{", "text": "", username": "' + username.value +'"}';
        var request = new XMLHttpRequest();
    	request.open("PUT", 'message/'+id, true);
    	request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    	request.onreadystatechange = function(){};
    	request.send(body);
    }
}

function PUT(){
    var id = document.getElementById('id').value;
    if (id != ""){
        var body = '{", "text": "' + text.value + '"username": "' + username.value +'"}';
        var request = new XMLHttpRequest();
    	request.open("PUT", 'message/'+id, true);
    	request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    	request.onreadystatechange = function(){};
    	request.send(body);
    }
}

function Delete(){
	var id = document.getElementById('id').value;
    if (id != ""){
        var request = new XMLHttpRequest();
        request.open("DELETE", 'message/'+id, true);
        request.send();
    }
}

